package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Map<String, String> banksInfo = new HashMap<>();
        banksInfo.put("melli", "603799");
        banksInfo.put("sepah", "589210");
        banksInfo.put("keshavarzi", "603770");
        banksInfo.put("melat", "610433");
        banksInfo.put("saderat", "603769");
        banksInfo.put("Tejarat", "627353");

        Scanner in = new Scanner(System.in);
        String[] digits = in.next().split("-");
        String result = digits[0] + digits[1].substring(0, 2);

        for (Map.Entry<String,String>entry:banksInfo.entrySet()) {
            if (entry.getValue().equals(result)){
                System.out.println(entry.getKey());
            }
        }
    }
}
